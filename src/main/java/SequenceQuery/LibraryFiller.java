/*
 * Copyright (c) 2019 Marian Hendriks [m.hendriks@st.hanze.nl].
 * All rights reserved.
 */
package SequenceQuery;

import java.util.*;

/**
 * This class parses the user given input file.
 * Throws errors and gives feedback when file is not fasta or not accessible
 * @author Marian Hendriks [m.hendriks@st.hanze.nl]
 * @version 1.0
 */
public class LibraryFiller {

    private final ArrayList<AminoAcid> AminoAcids;
    private final ArrayList<AminoAcid> DNA;
    private final ArrayList<AminoAcid> RNA;

    /**
     * Constructor for the LibrayFiller class
     */
    LibraryFiller(){
        this.AminoAcids = fillAminoAcids();
        this.DNA = fillDNA();
        this.RNA = fillRNA();
    }

    /**
     * Fills an ArrayList of AminoAcid objects with amino acids
     * @return list ArrayList of AminoAcids
     */
    private static ArrayList<AminoAcid> fillAminoAcids(){
        ArrayList<AminoAcid> list = new ArrayList<>();
        list.add(new AminoAcid('A', 89.0935));
        list.add(new AminoAcid('C', 121.1590));
        list.add(new AminoAcid('D', 133.1032));
        list.add(new AminoAcid('E', 147.1299));
        list.add(new AminoAcid('F', 165.1900));
        list.add(new AminoAcid('G', 75.0669));
        list.add(new AminoAcid('H', 155.1552));
        list.add(new AminoAcid('I', 131.1736));
        list.add(new AminoAcid('K', 146.1882));
        list.add(new AminoAcid('L', 131.1736));
        list.add(new AminoAcid('M', 149.2124));
        list.add(new AminoAcid('N', 132.1184));
        list.add(new AminoAcid('P', 115.1310));
        list.add(new AminoAcid('Q', 146.1451));
        list.add(new AminoAcid('R', 174.2017));
        list.add(new AminoAcid('S', 105.0930));
        list.add(new AminoAcid('T', 119.1197));
        list.add(new AminoAcid('V', 117.1469));
        list.add(new AminoAcid('W', 204.2262));
        list.add(new AminoAcid('Y', 181.1894));
        return list;
    }

    /**
     * Fills an ArrayList of AminoAcid objects with DNA nucleotides
     * @return list ArrayList of AminoAcids
     */
    private static ArrayList<AminoAcid> fillDNA(){
        ArrayList<AminoAcid> list = new ArrayList<>();
        list.add(new AminoAcid('A', 331.2218));
        list.add(new AminoAcid('T', 322.2085));
        list.add(new AminoAcid('C', 307.1971));
        list.add(new AminoAcid('G', 347.2212));
        list.add(new AminoAcid('R', 0));
        list.add(new AminoAcid('Y', 0));
        list.add(new AminoAcid('K', 0));
        list.add(new AminoAcid('M', 0));
        list.add(new AminoAcid('S', 0));
        list.add(new AminoAcid('W', 0));
        list.add(new AminoAcid('B', 0));
        list.add(new AminoAcid('D', 0));
        list.add(new AminoAcid('H', 0));
        list.add(new AminoAcid('V', 0));
        list.add(new AminoAcid('N', 0));
        list.add(new AminoAcid('-', 0));
        return list;
    }

    /**
     * Fills an ArrayList of AminoAcid objects with RNA nucleotides
     * @return list ArrayList of AminoAcids
     */
    private static ArrayList<AminoAcid> fillRNA(){
        ArrayList<AminoAcid> list = new ArrayList<>();
        list.add(new AminoAcid('A', 347.2));
        list.add(new AminoAcid('C', 323.2));
        list.add(new AminoAcid('G', 363.2));
        list.add(new AminoAcid('U', 324.2));
        list.add(new AminoAcid('R', 0));
        list.add(new AminoAcid('Y', 0));
        list.add(new AminoAcid('K', 0));
        list.add(new AminoAcid('M', 0));
        list.add(new AminoAcid('S', 0));
        list.add(new AminoAcid('W', 0));
        list.add(new AminoAcid('B', 0));
        list.add(new AminoAcid('D', 0));
        list.add(new AminoAcid('H', 0));
        list.add(new AminoAcid('V', 0));
        list.add(new AminoAcid('N', 0));
        list.add(new AminoAcid('-', 0));
        return list;
    }

    ArrayList<AminoAcid> getAminoAcids() {
        return AminoAcids;
    }

    ArrayList<AminoAcid> getDNA() {
        return DNA;
    }

    ArrayList<AminoAcid> getRNA() {
        return RNA;
    }

    @Override
    public String toString() {
        return "LibraryFiller{" +
                "AminoAcids=" + AminoAcids +
                ", DNA=" + DNA +
                ", RNA=" + RNA +
                '}';
    }
}

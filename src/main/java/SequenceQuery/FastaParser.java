/*
 * Copyright (c) 2019 Marian Hendriks [m.hendriks@st.hanze.nl].
 * All rights reserved.
 */
package SequenceQuery;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * This class parses the user given input file.
 * Throws errors and gives feedback when file is not fasta or not accessible
 * @author Marian Hendriks [m.hendriks@st.hanze.nl]
 * @version 1.0
 */
class FastaParser {

    /**
     * Parses input file and returns found fasta records in a List.
     * Will report if no fasta records have been found
     * Throws FileNotFoundException when file not found
     * Throws IOException when file can not be opened
     * @param fastaFile the file to parse
     * @return seqList A List with fasta record objects
     */
    List<FastaRecord> read_fasta(String fastaFile) {

        String line = "";
        String sequence = "";
        StringBuilder seq_container = new StringBuilder();
        String line_container = "";
        List<FastaRecord> seqList = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(fastaFile))) {
            String current_line;
            while ((current_line = br.readLine()) != null) {
                if(current_line.isEmpty()){
                    assert true; //skip when empty rows are present
                }
                else if(current_line.charAt(0) == '>') {
                    //only end concatting sequence after a new entry starts
                    sequence = seq_container.toString();
                    seq_container = new StringBuilder(); //reset container
                    line = line_container;
                    line_container = current_line;
                }
                else if(current_line.charAt(0) != '>') {
                    seq_container.append(current_line); //join sequence lines
                    seq_container = new StringBuilder((seq_container.toString().replaceAll("\\s", ""))); //remove whitespaces
                }
                if(!line.equals("") && !sequence.equals("")) {
                    seqList.add(new FastaRecord(line.trim(), sequence.length(), sequence, ""));
                }
                line = "";
            }
            //add the last entry manually as there wont be another loop
            if(!line_container.trim().equals("") && !seq_container.toString().equals("")) {
                seqList.add(new FastaRecord(line_container.trim(), seq_container.length(), seq_container.toString(), ""));
            }

        }catch (FileNotFoundException e){
            System.out.println("File " + fastaFile + " not found. Please check if the file path and names are correct.");
            System.exit(0);
        }catch (IOException e) {
            System.out.println("File " + fastaFile + " could not be opened. Please check if the file is accessible.");
            System.exit(0);
        }
        if(seqList.isEmpty()){
            System.out.println("No fasta records found in file " + fastaFile);
            System.exit(0);
        }
        return seqList;
    }
}


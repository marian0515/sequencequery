/*
 * Copyright (c) 2019 Marian Hendriks [m.hendriks@st.hanze.nl].
 * All rights reserved.
 */
package SequenceQuery;

import java.util.Objects;

/**
 * This class defines the FastaRecord object.
 * @author Marian Hendriks [m.hendriks@st.hanze.nl]
 * @version 1.0
 */
public class FastaRecord {
    private final String header;
    private final String sequence;
    private final int length;
    private final String type;

    /**
     * Constructor for FastaRecord class
     * @param header Fasta record header
     * @param length Fasta record sequence length
     * @param sequence Fasta record sequence
     * @param type Fasta record sequence type
     */
    FastaRecord(String header, int length, String sequence, String type) {
        this.header = header;
        this.length = length;
        this.sequence = sequence;
        this.type = type;
    }

    String getHeader() {
        return header;
    }

    String getSequence() {
        return sequence;
    }

    int getLength() {
        return length;
    }

    String getType() { return type;}

    @Override
    public String toString() {
        return "FastaRecord{" +
                "header='" + header + '\'' +
                ", sequence='" + sequence + '\'' +
                ", length=" + length +
                ", type='" + type + '\'' +
                '}';
    }

    /**
     * Override default equals implementation
     * @param o Object
     * @return boolean
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FastaRecord fastaRecord1 = (FastaRecord) o;
        return length == fastaRecord1.length &&
                Objects.equals(header, fastaRecord1.header) &&
                Objects.equals(sequence, fastaRecord1.sequence) &&
                Objects.equals(type, fastaRecord1.type);
    }

    /**
     * Override default hashcode implementation
     * @return int
     */
    @Override
    public int hashCode() {
        return Objects.hash(header, sequence, length, type);
    }
}

/*
 * Copyright (c) 2019 Marian Hendriks [m.hendriks@st.hanze.nl].
 * All rights reserved.
 */
package SequenceQuery;

import java.util.Objects;

/**
 * This class defines the AminoAcid object.
 * @author Marian Hendriks [m.hendriks@st.hanze.nl]
 * @version 1.0
 */
public class AminoAcid {
    private double molWeight;
    private char code;


    /**
     * Constructor for the AminoAcid class
     * @param code DNA/Protein molecule 1-letter code
     * @param molWeight Molecule weight
     */
    AminoAcid(char code, double molWeight) {
        this.molWeight = molWeight;
        this.code = code;
    }

    double getMolWeight() {
        return molWeight;
    }

    char getCode() {
        return code;
    }

    /**
     * Override default equals implementation
     * @param o Object
     * @return boolean
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AminoAcid aminoAcid = (AminoAcid) o;
        return Double.compare(aminoAcid.molWeight, molWeight) == 0 &&
                code == aminoAcid.code;
    }
    /**
     * Override default hashcode implementation
     * @return int
     */
    @Override
    public int hashCode() {
        return Objects.hash(molWeight, code);
    }

    @Override
    public String toString() {
        return "AminoAcid{" +
                "molWeight=" + molWeight +
                ", code=" + code +
                '}';
    }



}

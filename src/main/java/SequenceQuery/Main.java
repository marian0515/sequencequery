/*
 * Copyright (c) 2019 Marian Hendriks [m.hendriks@st.hanze.nl].
 * All rights reserved.
 */
package SequenceQuery;

import org.apache.commons.cli.*;
import java.util.*;

/**
 * This class is the main controller in the SequenceQuery application.
 * It contains the main() method and runs the filter steps
 * @author Marian Hendriks [m.hendriks@st.hanze.nl]
 * @version 1.0
 */
public class Main {

    /**
     * Main() method handles user input
     * throws ParseException when commandline input cannot de parsed
     * @param args user input commandline arguments
     */
    public static void main(String[] args) {
        CommandLineParser parser = new DefaultParser();
        Options options = setOptions(new Options());
        try{
            CommandLine line = parser.parse(options, args);
            processInput(line, options);
        }
        catch(ParseException exp){
            System.err.println( "Parsing failed.  Reason: " + exp.getMessage() + "\n" );
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp( "SequenceQuery", options, true );
        }
    }

    /**
     * This method parses the user input for all possible options available
     * Will call upon method printOutput for providing output
     * @param line user input parameters and program options
     * @param options list with possible input parameters
     */
    private static void processInput(CommandLine line, Options options){

        if(line.hasOption("help")){
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp( "SequenceQuery", options, true );
            //Please note, regex is case sensitive
        }
        else if(line.hasOption("input")){

            FastaParser fp = new FastaParser();
            List<FastaRecord> fastaList = fp.read_fasta(line.getOptionValue("input"));
            List<CsvRecord> csvList = new ArrayList<>();
            FastaFilter filter = new FastaFilter();

            if(line.hasOption("find_id")){
                fastaList = filter.filterId(fastaList, line.getOptionValue("find_id"));
                printOutput(fastaList, csvList, false, line.getOptionValue("input"), "");
            }
            else if (line.hasOption("find_prosite") || line.hasOption("find_regex") || line.hasOption("find_organism") || line.hasOption("find_description") ){

                if(line.hasOption("find_organism")){
                    fastaList = filter.filterOrganism(fastaList, line.getOptionValue("find_organism"));
                }
                if(line.hasOption("find_description")){
                    fastaList = filter.filterDescription(fastaList, line.getOptionValue("find_description"));
                }
                if(line.hasOption("find_prosite")){
                    csvList = filter.filterRegex(fastaList, line.getOptionValue("find_prosite"), false);
                }
                else if(line.hasOption("find_regex")){
                    csvList = filter.filterRegex(fastaList, line.getOptionValue("find_regex"), true);
                }
                if(csvList.isEmpty() && (line.hasOption("find_regex") || line.hasOption("find_prosite") )){
                    System.out.println("No fasta records left after filtering");
                    System.exit(0);
                }
                if(fastaList.isEmpty() ){
                    System.out.println("No fasta records left after filtering");
                    System.exit(0);
                }
                //als cvlist = o programma stoppen. als cvlist kleiner of gelijk is aan seqlist
                printOutput(fastaList, csvList, false, line.getOptionValue("input"), "");
            }
            else if(line.hasOption("summary")){
                csvList = filter.printCSV(fastaList);
                printOutput(fastaList, csvList, true, line.getOptionValue("input"), "");
            }
            else if(line.hasOption("to_csv")){
                csvList = filter.printCSV(fastaList);
                printOutput(fastaList, csvList, false, line.getOptionValue("input"), line.getOptionValue("to_csv"));
            }
            else{
                //when only a input file is specified
                HelpFormatter formatter = new HelpFormatter();
                formatter.printHelp( "SequenceQuery", options, true );
            }
        }
        else{
            System.out.println("No input file specified. \n");
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp( "SequenceQuery", options, true );
        }
    }

    /**
     * Provides filter results and output for the user
     * @param record FastaRecords left after performing filtersteps
     * @param csv CsvRecords left after performing filtersteps
     * @param summary indicates is option --summary is given
     * @param input filename of input file
     * @param separator seperator to use in printing csv file (--to_csv)
     */
    private static void printOutput(List<FastaRecord> record, List<CsvRecord> csv, boolean summary, String input, String separator){
        if(summary){
            List<String> types = new ArrayList<>();
            double avg_length = 0;

            for(CsvRecord csvRecord : csv){
                types.add(csvRecord.getType());
                avg_length += csvRecord.getLength();
            }
            double avg = Math.round(avg_length / record.size());

            HashSet<String> unique = new HashSet<>(types);
            StringBuilder unique_types = new StringBuilder();
            for(String type : unique){
                unique_types.append(type).append(" ");
            }

            System.out.println("file\t" + input.replaceFirst("^\\w+(/|\\\\)", ""));
            System.out.println("sequence types\t" + unique_types);
            System.out.println("number of sequences\t" + csv.size());
            System.out.println("average sequence length\t" + avg);
        }

        else if(!separator.equals("")){
            //print header
            System.out.println("ACCNO" + separator + "NAME" + separator + "ORGANISM" + separator + "TYPE" + separator + "LENGHT" + separator + "MOL_WEIGHT");

            for(CsvRecord csvRecord : csv){
                double molweight = (double) Math.round(csvRecord.getMol_weight() * 10) / 10; //cast long to double
                System.out.println(csvRecord.getAacno() + separator + csvRecord.getName() + separator +
                        csvRecord.getOrganism() + separator + csvRecord.getType() + separator + csvRecord.getLength() +
                        separator + molweight );
            }
        }

        else if(csv.isEmpty()){ //csv is never used, use fasta format
            for(FastaRecord fastaRecord : record){
                System.out.println(fastaRecord.getHeader());
                System.out.println(fastaRecord.getSequence().replaceAll("(.{70})", "$1\n")); //linebreak every 70 chars
            }
        }
        else{
            separator = "\t";
            System.out.println("ACCNO" + separator + "NAME" + separator + "ORGANISM" + separator + "TYPE" + separator + "POSITION" + separator + "SEQ");
            for(CsvRecord csvRecord : csv){
                System.out.println(csvRecord.getAacno() + separator + csvRecord.getName() + separator +
                        csvRecord.getOrganism() + separator + csvRecord.getType() + separator + csvRecord.getPosition() + separator + csvRecord.getSeq());
            }
        }
    }

    /**
     * Sets the possible usecases for the SequenceQuery application
     * @param options Apache CLI options object
     * @return options list with possible input parameters
     */
    private static Options setOptions(Options options){
        options.addOption("i","input",true,"Fasta file to use (.fa/.faa/.fna).");
        options.addOption("s","summary",false,
                "Creates a textual summary of the parsed file: number of sequences, sequence type, average sequence length.");
        options.addOption("h","help",false,"Shows help information.");
        options.addOption("c","to_csv",true,"Prints fasta records in csv format. A delimiter must be supplied.");
        options.addOption("p", "find_prosite", true,"Will report all sequences that contain the prosite pattern with the location and actual sequence found. Details will be separated using a tab.");
        options.addOption("r","find_regex",true,"Will report all sequences that contain the regular expression pattern," +
                "with the location and actual sequence found. The pattern is case sensitive. Details will be separated using a ta.b");
        options.addOption("o","find_organism",true,"Will report all sequences having this regex pattern in the sequence name organism if found. The pattern is case sensitive.\"");
        options.addOption("d","find_description",true,"Will report all sequences having this regex pattern in the sequence name description or sequence name. The pattern is case sensitive.");
        options.addOption("e","find_id",true,"Returns fasta records matching the given id.");
        return options;
    }

}
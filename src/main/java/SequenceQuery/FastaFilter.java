/*
 * Copyright (c) 2019 Marian Hendriks [m.hendriks@st.hanze.nl].
 * All rights reserved.
 */
package SequenceQuery;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * This class processes the user input for the different filter steps.
 * @author Marian Hendriks [m.hendriks@st.hanze.nl]
 * @version 1.0
 */
class FastaFilter {

    /**
     * Filters on id (--find_id) as found in the fasta record header
     * Returns all fasta records containing this id
     * @param fastaRecords fasta records to filter
     * @param id user input featuring a fatsa header ID
     * @return filtered_sequences fasta records left after filtering on id
     */
    List<FastaRecord> filterId(List<FastaRecord> fastaRecords, String id){
        List<FastaRecord> filtered_fastaRecords = new ArrayList<>();
        for(FastaRecord fastaRecord : fastaRecords){
            id = id.toLowerCase(); //Make sure caps are irrelevant in searching
            if(fastaRecord.getHeader().toLowerCase().contains(id)){
                filtered_fastaRecords.add(new FastaRecord(fastaRecord.getHeader(), fastaRecord.getLength(), fastaRecord.getSequence(), getType(fastaRecord.getSequence())));
            }
        }
        return filtered_fastaRecords;
    }

    /**
     * Filters on organism name(--find_organism) as found in the fasta record header
     * Returns all fasta records matching the organism if the input is valid regex
     * @param fastaRecords fasta records to filter
     * @param organism user provided regex pattern containing (a part of) the organism name
     * @return filtered_sequences fasta records left after filtering on organism
     */
    List<FastaRecord> filterOrganism(List<FastaRecord> fastaRecords, String organism){
        if(isValidRegex(organism)){
            List<FastaRecord> filtered_fastaRecords = new ArrayList<>();
            for(FastaRecord fastaRecord : fastaRecords){
                if(fastaRecord.getHeader().toLowerCase().matches("(.*)\\[(.*)" + organism.toLowerCase() + "(.*)\\](.*)")){
                    filtered_fastaRecords.add(new FastaRecord(fastaRecord.getHeader(), fastaRecord.getLength(), fastaRecord.getSequence(), getType(fastaRecord.getSequence())));
                }
            }
            return filtered_fastaRecords;
        }
        else{
            System.out.println("Input: \"" + organism + "\" for --find_organism is not a valid regex pattern. \n" +
                    "This filter will not be applied. \n");
            return fastaRecords;
        }
    }

    /**
     * Filters on description name(--find_description) as found in the fasta record header
     * Will search complete header if header is not ncbi formatted
     * Returns all fasta records matching the organism if the input is valid regex
     * @param fastaRecords fasta records to filter
     * @param description user provided regex pattern containing (a part of) the description
     * @return filtered_sequences fasta records left after filtering on organism
     */
    List<FastaRecord> filterDescription(List<FastaRecord> fastaRecords, String description){
        if(isValidRegex(description)) {
            List<FastaRecord> filtered_fastaRecords = new ArrayList<>();
            for (FastaRecord fastaRecord : fastaRecords) {
                if (fastaRecord.getHeader().toLowerCase().matches(">.*(\\|)(?!.*\\|).*" + description.toLowerCase() + ".*\\[.*\\].*")) {
                    filtered_fastaRecords.add(new FastaRecord(fastaRecord.getHeader(), fastaRecord.getLength(), fastaRecord.getSequence(), getType(fastaRecord.getSequence())));
                } else if (fastaRecord.getHeader().toLowerCase().matches(">.*" + description + ".*")) {
                    //When fasta header is not formatted clearly, use complete header
                    filtered_fastaRecords.add(new FastaRecord(fastaRecord.getHeader(), fastaRecord.getLength(), fastaRecord.getSequence(), getType(fastaRecord.getSequence())));
                }
            }
            return filtered_fastaRecords;
        }
        else{
            System.out.println("Input: \"" + description + "\" for --find_description is not a valid regex pattern. \n" +
                    "This filter will not be applied. \n");
            return fastaRecords;
        }
    }

    /**
     * Transforms FastaRecords into CsvRecords
     * Returns all fasta records in an alternative format of CsvRecords
     * @param fastaRecords fasta records to filter
     * @return csv_sequences list of CsvRecords
     */
    List<CsvRecord> printCSV(List<FastaRecord> fastaRecords){
        List<CsvRecord> csv_sequences = new ArrayList<>();

        for(FastaRecord fastaRecord : fastaRecords){
            String seq = fastaRecord.getSequence();
            String head = fastaRecord.getHeader();
            int length = (seq.length());
            String type = getType(seq);
            double molWeight = getMolWeight(seq, type);

            Pattern ncbi_header = Pattern.compile("^>([^\\|]*\\|[^\\|]*).*\\|(.*)\\[(.*)\\].*");
            Pattern alternative_header = Pattern.compile("^>(\\S+).*");
             Matcher matcher = ncbi_header.matcher(head);
             Matcher matcher_alt = alternative_header.matcher(head);

             if(matcher.find()){
                 csv_sequences.add(new CsvRecord(matcher.group(1).trim(), matcher.group(2).trim(), matcher.group(3).trim(), type, length, molWeight, 0, ""));
             }
             else if(matcher_alt.find()){
                 //if there is a clear accesion code in alternative, isolate it
                Pattern acc_header = Pattern.compile("^([^\\|]*\\|[^\\|]*).*");
                Matcher matcher_alt_acc = acc_header.matcher(matcher_alt.group(1));

                if(matcher_alt_acc.find()){
                csv_sequences.add(new CsvRecord(matcher_alt_acc.group(1).trim(), "", "", type, length, molWeight, 0, ""));
                }
                else{
                    csv_sequences.add(new CsvRecord(matcher_alt.group(1).trim(), "", "", type, length, molWeight, 0, ""));
                }
             }
        }
        return csv_sequences;
    }

    /**
     * Filters on prosite or regex pattern (--find_regex, --find_prosite)
     * Will check if patterns are valid before filtering and will return an
     * error message if this is the case
     * Returns all found FastaRecords as a CsvRecord
     * @param fastaRecords fasta records to filter
     * @param user_input user input featuring prosite or regex pattern
     * @param regex flag indicating if the prosite or regex pattern should be used
     * @return csv_sequences list of CsvRecords
     */
    List<CsvRecord> filterRegex(List<FastaRecord> fastaRecords, String user_input, Boolean regex){
        List<CsvRecord> csv_sequences = new ArrayList<>();
        String pattern;
        int position;

        if(regex) {
            pattern = user_input;
        }
        else{
            pattern = prositeToRegex(user_input);
        }

        if(isValidRegex(pattern)) {

            pattern = pattern.toUpperCase();

            for (FastaRecord fastaRecord : fastaRecords) {
                String seq = fastaRecord.getSequence().toUpperCase();
                String head = fastaRecord.getHeader();
                int length = (seq.length());
                Pattern p = Pattern.compile(pattern);
                Matcher m = p.matcher(seq);

                if (m.find()) {

                    String type = getType(seq);
                    double molWeight = getMolWeight(seq, type); //get molweight for each sequence
                    position = m.start() + 1;
                    seq = m.group();

                    Pattern ncbi_header = Pattern.compile("^>([^\\|]*\\|[^\\|]*).*\\|(.*)\\[(.*)\\].*");
                    Pattern alternative_header = Pattern.compile("^>(\\S+).*");
                    Matcher matcher = ncbi_header.matcher(head);
                    Matcher matcher_alt = alternative_header.matcher(head);

                    if (matcher.find()) {
                        csv_sequences.add(new CsvRecord(matcher.group(1).trim(), matcher.group(2).trim(), matcher.group(3).trim(), type, length, molWeight, position, seq));
                    } else if (matcher_alt.find()) {
                        //if there is a clear accesion code in non ncbi string, isolate it
                        Pattern acc_header = Pattern.compile("^([^\\|]*\\|[^\\|]*).*");
                        Matcher matcher_alt_acc = acc_header.matcher(matcher_alt.group(1));
                        if (matcher_alt_acc.find()) {
                            csv_sequences.add(new CsvRecord(matcher_alt_acc.group(1).trim(), "", "", type, length, molWeight, position, seq));
                        } else {
                            csv_sequences.add(new CsvRecord(matcher_alt.group(1).trim(), "", "", type, length, molWeight, position, seq));
                        }
                    }
                }
            }
            return csv_sequences;
        }
        else{
            if(regex){
                System.out.println("Input: \"" + pattern + "\" for --find_regex is not a valid regex pattern. \n" +
                        "This filter will not be applied.\n");
                System.exit(0);
            }
            else{
                System.out.println("Input: \"" + pattern + "\" for --find_prosite could not be translated into a proper regex pattern. \n" +
                        "This filter will not be applied. \n");
                System.exit(0);
            }
            return csv_sequences;
        }
    }

    /**
     * Method calculates molecular weight of protein/rna/dna sequence
     * @param sequence the fasta sequence to calculate
     * @param type the type of the fasta sequence (protein/rna/dna)
     * @return sum, the total molweight of sequence
     */
    private double getMolWeight(String sequence, String type){
        double sum = 18.01528; //compensate for H2O
        List<AminoAcid> list = new ArrayList<>();
        sequence = sequence.toUpperCase(); //AA are in upper case

        if(type.equals("PROTEIN")){list = new LibraryFiller().getAminoAcids();}
        if(type.equals("DNA")){list = new LibraryFiller().getRNA();}
        if(type.equals("RNA")){list = new LibraryFiller().getDNA();}

        for(char c : sequence.toCharArray()){
            for(AminoAcid aa : list ){
                if(c == aa.getCode() && type.equals("PROTEIN")){
                    sum += (aa.getMolWeight() - 18.01528); //correct for H2O lost in peptide bonding
                }
                else if(c == aa.getCode()){ //if DNA or RNA
                    sum += (aa.getMolWeight());
                }
           }
        }
        if(type.equals("PROTEIN")){
            return sum;
        }
        else{
            return sum - 18.01528; //no H2O correction in DNA/RNA
        }
    }

    /**
     * This method takes a prosite pattern and transforms it into a regex pattern
     * Returns the translated prosite pattern
     * @param prosite featuring prosite
     * @return prosite, prosite converted to regex
     */
    private String prositeToRegex(String prosite){

        if(prosite.matches("<\\{[A-Z]\\}\\*>")){
            Pattern pattern = Pattern.compile("<\\{([A-Z])\\}\\*>");
            Matcher matcher = pattern.matcher(prosite);
            if(matcher.find()){
                prosite = "^(?:(?!" + matcher.group(1) + ").)*$\\r?\\n?";
            }
        }
        else{
            prosite = prosite.replace(" ", "");
            prosite = prosite.replace("x", ".");
            prosite = prosite.replace("-", "");
            prosite = prosite.replace("<", "^");
            prosite = prosite.replace(">", "$");
            prosite = prosite.replace("(", "{");
            prosite = prosite.replace(")", "}");
        }
        return prosite;
    }

    /**
     * Method determines the type of the fasta sequence
     * Returns the found type or "Type not found" if it cant be determined
     * @param sequence Fasta record sequence
     * @return tpe (DNA/RNA/PROTEIN)
     */
    private String getType(String sequence){
        if(sequence.toUpperCase().matches("[ACGTRYKMSWBDHVN\\-]+")){
            return "DNA";
        }
        else if(sequence.toUpperCase().matches("[ACGURYKMSWBDHVN\\-]+")){
            return "RNA";
        }
        else if(sequence.toUpperCase().matches("[[A-Z]\\*-]+")){
            return "PROTEIN";
        }
        else{
            return "Type not found";
        }
    }

    /**
     * Checks whether a regex pattern is valid
     * Throws PatternSyntaxException if regex pattern will not compile
     * @param option regex string to check
     * @return boolean true is regex is valid
     */
    private boolean isValidRegex(String option){
        try{
            Pattern.compile(option);
            return true;
        }catch(PatternSyntaxException e){
            return false;
        }
    }
}

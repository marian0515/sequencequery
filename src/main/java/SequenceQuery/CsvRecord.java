/*
 * Copyright (c) 2019 Marian Hendriks [m.hendriks@st.hanze.nl].
 * All rights reserved.
 */
package SequenceQuery;

import java.util.Objects;

/**
 * This class defines the CsvRecord object.
 * @author Marian Hendriks [m.hendriks@st.hanze.nl]
 * @version 1.0
 */
public class CsvRecord {
    private final String aacno;
    private final String name;
    private final String organism;
    private final String type;
    private final int length;
    private final double mol_weight;
    private final int position;
    private final String seq;

    /**
     * Constructor for CsvRecord class
     * @param aacno (First) acces code from fasta header
     * @param name Fasta sequence description or name from header
     * @param organism Organism name from fasta header
     * @param type FastaRecord type
     * @param length FastaRecord length
     * @param mol_weight FastaRecord molecular weight
     * @param position Position of sequence match
     * @param seq FastaRecord of sequence match
     */
    CsvRecord(String aacno, String name, String organism, String type, int length, double mol_weight, int position, String seq) {
        this.aacno = aacno;
        this.name = name;
        this.organism = organism;
        this.type = type;
        this.length = length;
        this.mol_weight = mol_weight;
        this.position = position;
        this.seq = seq;
    }

    String getAacno() {
        return aacno;
    }

    String getName() {
        return name;
    }

    String getOrganism() {
        return organism;
    }

    String getType() {
        return type;
    }

    int getLength() {
        return length;
    }

    double getMol_weight() {
        return mol_weight;
    }

    int getPosition() {
        return position;
    }

    String getSeq() {
        return seq;
    }

    @Override
    public String toString() {
        return "CsvRecord{" +
                "aacno='" + aacno + '\'' +
                ", name='" + name + '\'' +
                ", organism='" + organism + '\'' +
                ", type='" + type + '\'' +
                ", length=" + length +
                ", mol_weight=" + mol_weight +
                ", position=" + position +
                ", seq='" + seq + '\'' +
                '}';
    }


    /**
     * Override default equals implementation
     * @param o Object
     * @return boolean
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CsvRecord csvRecord = (CsvRecord) o;
        return length == csvRecord.length &&
                Double.compare(csvRecord.mol_weight, mol_weight) == 0 &&
                position == csvRecord.position &&
                Objects.equals(aacno, csvRecord.aacno) &&
                Objects.equals(name, csvRecord.name) &&
                Objects.equals(organism, csvRecord.organism) &&
                Objects.equals(type, csvRecord.type) &&
                Objects.equals(seq, csvRecord.seq);
    }
    /**
     * Override default hashcode implementation
     * @return int
     */
    @Override
    public int hashCode() {
        return Objects.hash(aacno, name, organism, type, length, mol_weight, position, seq);
    }
}

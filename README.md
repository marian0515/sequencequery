# SequenceQuery

SequenceQuery is an application written as part of a introductory course to Java.
This application will process FASTA format files (https://en.wikipedia.org/wiki/FASTA_format) and results will be reported to the user.
The FASTA files can be processed in different ways including filtering on the FASTA record header or sequence.

## Getting Started

This application is built with Gradle (version 5.1.1) and the .jar file used  JDK 11 as JVM. The Apache Commons CLI library is used for input parsing.
This guide will specify how to use this application.

To run this program as a java application (.jar), a recent installation of the Java Runtime Environment is needed (Java(TM) SE Runtime Environment 18.9 was used in testing).
Dependencies needed are included.
The .jar for this application can be found at:
```
https://bitbucket.org/marian0515/sequencequery/src/master/build/libs/SequenceQuery-Fatjar-1.1-SNAPSHOT.jar
```

This application can also be imported as a Gradle project. Pull the repository using git or the download the complete repository from the Bitbucket repository's downloads section.
When using IntelliJ; create a new project from existing folders and inside the repository folder, select the build.gradle file to import the project.


### Usage

The application can be run using different settings.
When a input variable is not given or when the -h/--help parameter us used, usage information will be shown:

```
java -jar SequenceQuery-Fatjar-1.1-SNAPSHOT.jar

usage: SequenceQuery [-c <arg>] [-d <arg>] [-e <arg>] [-h] [-i <arg>] [-o
       <arg>] [-p <arg>] [-r <arg>] [-s]
 -c,--to_csv <arg>             Prints fasta records in csv format. A
                               delimiter must be supplied.
 -d,--find_description <arg>   Will report all sequences having this regex
                               pattern in the sequence name description or
                               sequence name. The pattern is case
                               sensitive.
 -e,--find_id <arg>            Returns fasta records matching the given
                               id.
 -h,--help                     Shows help information.
 -i,--input <arg>              Fasta file to use (.fa/.faa/.fna).
 -o,--find_organism <arg>      Will report all sequences having this regex
                               pattern in the sequence name organism if
                               found. The pattern is case sensitive."
 -p,--find_prosite <arg>       Will report all sequences that contain the
                               prosite pattern with the location and
                               actual sequence found. Details will be
                               separated using a tab.
 -r,--find_regex <arg>         Will report all sequences that contain the
                               regular expression pattern,with the
                               location and actual sequence found. The
                               pattern is case sensitive. Details will be
                               separated using a ta.b
 -s,--summary                  Creates a textual summary of the parsed
                               file: number of sequences, sequence type,
                               average sequence length.
```


Filter steps can also be combined in queries:

```
java -jar SequenceQuery-Fatjar-1.1-SNAPSHOT.jar --input data/mixed.fasta --find_description "protein" --find_regex "GQT[AV]""
java -jar SequenceQuery-Fatjar-1.1-SNAPSHOT.jar --input data/fhit.faa --find_organism "Rattus" --find_description "histidine"
```

Results, when using filtersteps, will be displayed either in FASTA format. (When using only --find_id, --find_description, and/or --find_organism)
```
>gi|9587672|gb|AAF89328.1|AF170064_1 fragile histidine triad protein [Rattus norvegicus]
MSFKFGQHLIKPSVVFLKTELSFALVNRKPVVPGHVLMCPLRPVERFRDLRPDEVADLFQVTQRVGTVVE
KHFQGTSITFSMQDGPEAGQTVKHVHVHILPRKSGDFRRNDNIYDELQKHDREEEDSPAFWRSEEEMAAE
AEVLRAYFQA
>gi|149040065|gb|EDL94149.1| fragile histidine triad gene, isoform CRA_a [Rattus norvegicus]
MSFRFGQHLIKPSVVFLKTELSFALVNRKPVVPGHVLVCPLRPVERFRDLRPDEVADLFQVTQRVGTVVE
KHFQGTSITFSMQDGPEAGQTVKHVHVHILPRKSGDFRRNDNIYDELQKHDREEEDSPAFWRSEEEMAAE
AEVLRAYFQA
```

Or they will returned in a tab-delimited CSV format wih the matching sequence and its location. (When using the description or orgamism filter with --find_regex and/or --find_prosite)
Please note that not mall columns may be filled out when printing in CSV mode. If a FASTA record header is not formatted clearly not all information may be extracted.
```
ACCNO	NAME	ORGANISM	TYPE	POSITION	SEQ
gi|9587672	AF170064_1 fragile histidine triad protein	Rattus norvegicus	PROTEIN	89	GQTV
gi|11120730	bis(5'-adenosyl)-triphosphatase	Rattus norvegicus	PROTEIN	89	GQTV
gi|149040065	fragile histidine triad gene, isoform CRA_a	Rattus norvegicus	PROTEIN	89	GQTV
gi|149040064	fragile histidine triad gene, isoform CRA_a	Rattus norvegicus	PROTEIN	89	GQTV
```

### Test data

Test data for this application can be found in the folder:
https://bitbucket.org/marian0515/sequencequery/src/master/data

### Documentation

Javadoc for this application can be found in the folder:
https://bitbucket.org/marian0515/sequencequery/src/master/documentation


## Built With

* [IntelliJ IDEA (version 2018.3)](http://www.dropwizard.io/1.0.2/docs/) - IDE used
* [Apache Commons CLI (version 1.4)](https://commons.apache.org/proper/commons-cli/index.html) - Java library for parsing command line options
* [Gradle (version 5.1.1)](https://gradle.org/) - Build and deployment tool

